require "./entity"

module Crystalyser

  class Character < Entity
    # Types of attributes
    alias AbilityUnit = UInt8 | Nil
    alias SkillValue = Int8 | Nil
    alias HitDiceValue = UInt32
    alias SpeedValue = UInt8 | Nil
    alias ReachValue = UInt8
    alias SaveValue = UInt8
    alias FeatData = Hash(String, String?)

    # Attributes annotations
    @name       : String
    @ba         : Int8
    @abilities  : Hash(ABILITIES, AbilityUnit)
    @skills     : Hash(String, SkillValue)
    @hit_points : HitDiceValue
    @speeds     : Hash(SPEEDS, SpeedValue)
    @space      : SPACES
    @reach      : ReachValue
    @saves      : Hash(SAVES, SaveValue)
    @feats      : Hash(Component::Id, FeatData) # {1 => {"name" => "Run faster", "type" => "sur"}}. if value is nil, then it's a default infos of the feat
    @effects    : Array(Effect) # stores race modifiers, ...

    # Getters and setters
    getter abilities, name, ba, abilities, skills, hit_points, speeds, space, reach, saves, feats, effects
    setter abilities, name, ba, abilities, skills, hit_points, speeds, space, reach, saves, feats, effects

    TILE_TO_METER = ->(n) { (n * 1.5) }

    def initialize(@name, id = nil, ba = nil, abilities = nil, skills = nil, hit_points = nil, speeds = nil, space = nil, reach = nil, saves = nil, feats = nil, effects = nil)
      @ba = ba || 0_i8
      @abilities = abilities || {
        ABILITIES::STR => 10_u8,
        ABILITIES::DEX => 10_u8,
        ABILITIES::CON => 10_u8,
        ABILITIES::INT => 10_u8,
        ABILITIES::WIS => 10_u8,
        ABILITIES::CHA => 10_u8,
      } of ABILITIES => AbilityUnit
      @skills = skills || {} of String => SkillValue
      @hit_points = hit_points || 1_u32
      @speeds = speeds || {
        SPEEDS::GROUND  => 6_u8,
        SPEEDS::WATER   => 2_u8,
        SPEEDS::CLIMB   => 2_u8,
        SPEEDS::AIR     => nil,
      } of SPEEDS => SpeedValue
      @space = space || SPACES::M
      @reach = reach || 1_u8
      @saves = saves || {
        SAVES::FORT => 0_u8,
        SAVES::REF  => 0_u8,
        SAVES::WILL => 0_u8,
      } of SAVES => SaveValue
      @feats = feats || {} of Feat::Id => FeatData
      @effects = effects || [] of Effect
      super()
    end
  end

end

require "./character/*"

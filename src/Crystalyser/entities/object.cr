require "./entity"
module Crystalyser

  class Object < Entity

    @name : String

    def initialize(@name)
      super()
    end
  end

end

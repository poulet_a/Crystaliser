require "./object"
module Crystalyser

  class Weapon < Object

    def initialize(name, @damage, @type)
      super(name)
    end
  end

end

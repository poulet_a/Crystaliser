module Crystalyser

  class Entity
    alias Id = UInt32

    @@entity_id : Id
    @entity_id  : Id

    getter entity_id

    @@entity_id = 0_u32
    def initialize
      @entity_id = Entity.new_id
    end

    def self.new_id
      @@entity_id += 1
    end
  end

end

require "./component"
module Crystalyser

  class Attack < Component

    @id :             IDS
    @value_bonus :    Int8
    @value_dmg :      Int8
    @value_ability :  Character::ABILITIES | Skill::IDS
    @ability :        Character::ABILITIES
    @type :           TYPES
    @lethality :      Bool
    @contact :        Bool

    def initialize(@id, @value_bonus, @value_dmg, @value_ability, @ability, @type, @lethality = true, @contact = true)
      super()
    end
  end

end

require "./attack/*"

require "./system"

module Crystalyser

  class Dice < System

      def self.roll(dice_type = 20, dice_number = 1, modifier = 0, random = Random.new)
        (1..dice_number).reduce(modifier) do |prev|
          prev + random.rand 1..dice_type
        end
      end

  end

end

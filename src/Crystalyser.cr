require "./Crystalyser/*"
require "./Crystalyser/components/*"
require "./Crystalyser/entities/*"
require "./Crystalyser/systems/*"

module Crystalyser
  def self.start
  end
end

Crystalyser.start()

# Crystalyser

A pathfinder like RPG.
The aim is to create an AI in a complex context.

## Requirements

- Ruby and rake
- Crystal 0.16

## Installation


```sh
make # build the project
make doc # install the documentation. Entry point at doc/index.html
make test # run the unitary tests
```

## Usage

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it ( https://github.com/Nephos/Crystalyser/fork )
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create a new Pull Request

## Contributors

- [pouleta](https://github.com/Nephos) Arthur Poulet - creator, maintainer
- [damaia](https://gitlab.com/u/Damaia) Lucie Dispot - creator, maintainer

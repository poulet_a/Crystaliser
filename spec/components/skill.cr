describe Crystalyser::Inventory do

  it "works" do
    c = Crystalyser::Character.new name: "Camille"
    e = Crystalyser::Inventory.new owner: c
    (e.component_id).should_not eq(0)
  end

end

describe Crystalyser::Effect do

  it "works" do
    p = ->(c : Crystalyser::Character) { c }
    e = Crystalyser::Effect.new p, p, p, p, p, p
    (e.entity_id).should_not eq(0)
  end

end
